var express = require('express');
var router = express.Router();


var account_controller = require('../controllers/account');

/* GET home page. */
router.get('/',account_controller.userAccount);

module.exports = router;
